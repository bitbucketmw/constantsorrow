/*
 * position_component.h
 *
 *  Created on: 21 Apr 2013
 *      Author: adam
 */

#pragma once

namespace Desper
{
	class PositionComponent : public artemis::Component
	{
	public:
		float posX;
		float posY;
		PositionComponent(float posX, float posY)
		{
			this->posX = posX;
			this->posY = posY;
		};
	};
}
