/*
 * voronoi_map.h
 *
 *  Created on: 20 Apr 2013
 *      Author: adam
 */

#pragma once

#include <vector>
#include <random>
#include <ctime>

#include "precompiled.h"
#include "lib/voronoi/voronoi.h"

namespace Desper
{

class VoronoiMap
{
public:
    VoronoiMap(int num_points, int width, int height, int relaxCount = 3);
    void Draw(glhelper::GlProgram program, glutil::MatrixStack modelMatrix);
    void Update(float dt);

    class Center;
    class Edge;
    class Corner;

    class Center
    {
    public:
        Center(glm::vec4 coords);
        const glm::vec4 &getCoords();
        const std::vector<std::shared_ptr<const Center>> getNeighbours() const;
        const std::vector<std::shared_ptr<const Edge>> getBorders() const;
        const std::vector<std::shared_ptr<const Corner>> getCorners() const;
        void AddBorder(std::shared_ptr<Edge> border);
    private:
        glm::vec4 m_coords;
        std::vector<std::shared_ptr<const Center>> m_neighbours;
        std::vector<std::shared_ptr<const Edge>> m_borders;
        std::vector<std::shared_ptr<const Corner>> m_corners;
    };

    class Edge
    {
    public:
        Edge(std::shared_ptr<Corner> v0, std::shared_ptr<Corner> v1);
        // delaunay edges
        std::shared_ptr<const Center> getd0() const;
        std::shared_ptr<const Center> getd1() const;

        // voronoi edges
        std::shared_ptr<const Corner> getv0() const;
        std::shared_ptr<const Corner> getv1() const;

    private:
        std::shared_ptr<const Center> m_d0;
        std::shared_ptr<const Center> m_d1;

        std::shared_ptr<const Corner> m_v0;
        std::shared_ptr<const Corner> m_v1;
    };

    class Corner
    {
    public:
        Corner(glm::vec4 coord);
        const std::vector<std::shared_ptr<const Center>> getTouches() const;
        const std::vector<std::shared_ptr<const Edge>> getProtrudes() const;
        const std::vector<std::shared_ptr<const Corner>> getAdjacent() const;
    private:
        glm::vec4 m_coord;
        std::vector<std::shared_ptr<const Center>> m_touches;
        std::vector<std::shared_ptr<const Edge>> m_protrudes;
        std::vector<std::shared_ptr<const Corner>> m_adjacent;
    };

private:
    // Rng
    typedef std::mt19937 rng_type;
    rng_type m_rng;
    // Rng end

    glhelper::Mesh m_edgeMesh = glhelper::Mesh();
    glhelper::Mesh m_centerMesh = glhelper::Mesh();

    std::vector<Center> m_polygonCenters;

    std::vector<glm::vec2> GetCenters(std::list<voronoi::VSite> sites);
};

}
