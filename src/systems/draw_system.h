/*
 * draw_system.h
 *
 *  Created on: 21 Apr 2013
 *      Author: adam
 */

#pragma once

#include "../components/position_component.h"

namespace Desper
{
	class DrawSystem : public artemis::EntityProcessingSystem {
	private:
		artemis::ComponentMapper<PositionComponent> positionMapper;

	public:
		DrawSystem() {
			addComponentType<PositionComponent>();
		};

		virtual void initialize() {
			positionMapper.init(*world);
		};

		virtual void processEntity(artemis::Entity &e) {
			positionMapper.get(e)->posX;
			world->getDelta();
		};

	};
}
