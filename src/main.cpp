/*
 * main.cpp
 *
 * NB: http://stackoverflow.com/questions/2691107/opengl-glew-in-eclipse-for-windows
 *
 *  Created on: 19 Apr 2013
 *      Author: adam
 */

#include <GL/glew.h>
#include <GL/glfw.h>
#include <GL/glu.h>

#include "constants.h"
#include "lib/artemis/World.h"
#include "glhelper/program.h"
#include "lib/glutil/matrix_stack.h"
#include "voronoi_map.h"

const int RESOLUTION_X = 640;
const int RESOLUTION_Y = 480;

template<typename T, size_t sizeOfArray>
constexpr size_t ARRAY_COUNT(T (&)[sizeOfArray])
{
	return sizeOfArray;
}

const float vertexData[] =
{
	0.25f, 0.25f, 0.0f, 1.0f,
	0.25f, -0.25f, 0.0f, 1.0f,
	-0.25f, -0.25f, 0.0f, 1.0f,
};

const GLshort indexData[] =
{
		0, 1, 2
};

GLuint vbo;
GLuint vao;
GLuint ibo;

glhelper::GlProgram mainProgram;
glhelper::Camera mainCamera = glhelper::Camera(20.0f, 1.0f, 600.0f);

float lastTime = 0.0f;

// temp
Desper::VoronoiMap *m_vMap;
// end temp

void GlInit()
{
    mainProgram = glhelper::GlProgram(
            constants::RES_DIRECTORY + "shaders/pos_color_world_transform_UBO.vert", constants::RES_DIRECTORY + "shaders/color_mult_uniform.frag");
    mainProgram.UseCamera(mainCamera);

    glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indexData), indexData, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

	glBindVertexArray(0);

}

void Draw(glutil::MatrixStack modelMatrix)
{
    m_vMap->Draw(mainProgram, modelMatrix);
}

void GLFWCALL WindowResize(int width, int height)
{
    glm::mat4x4 persMatrix = glm::perspective(20.0f, ((float)width/height), mainCamera.m_zNear, mainCamera.m_zFar);

    glBindBuffer(GL_UNIFORM_BUFFER, mainProgram.GetGlobalMatricesUBO());
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), glm::value_ptr(persMatrix));
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

	glViewport(0, 0, (GLsizei) width, (GLsizei) height);
}

void ArtemisInit()
{
}

int main(int argc, char **argv) {
	// initialize glfw
	if (!glfwInit())
		return -1;

	// create a windowed mode window and its opengl context
	if (!glfwOpenWindow(RESOLUTION_X, RESOLUTION_Y, 8, 8, 8, 0, 24, 0, GLFW_WINDOW))
		return -1;

	glewInit();

	GlInit();

	artemis::World world;
	artemis::SystemManager *sysMgr = world.getSystemManager();

	artemis::EntityManager *entityMgr = world.getEntityManager();

    m_vMap = new Desper::VoronoiMap(200, 2, 2, 3);

    glfwSetWindowSizeCallback(WindowResize);
	// loop until the user closes the window
	while (glfwGetWindowParam(GLFW_OPENED))
	{
		float currentTime = glfwGetTime();
		float dt = currentTime - lastTime;
		lastTime = currentTime;

		// update here
        m_vMap->Update(dt);

        // temp
        mainCamera.ApplyTransform(glm::rotate(glm::mat4(1), 0.5f, glm::vec3(0, 1, 0)));
        // temp end

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);

        // temp
        glUseProgram(mainProgram.GetProgram());
        glBindBuffer(GL_UNIFORM_BUFFER, mainProgram.GetGlobalMatricesUBO());
        glm::mat4 cameraMat = mainCamera.GetWorldToCameraMat();
        glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(cameraMat));
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
        // temp end

		// render here
        glutil::MatrixStack modelMatrix;
        modelMatrix.Translate(-1, 0, -1);
        Draw(modelMatrix);

		// swap front and back buffers and process events
		glfwSwapBuffers();
	}

	glfwTerminate();

	return 0;
}
