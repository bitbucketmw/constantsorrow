#pragma once
#ifndef PRECOMPILED_H
#define PRECOMPILED_H

#include <memory>
#include <GL/glew.h>
#include <GL/glfw.h>
#include <GL/glu.h>
#include <glm/glm.hpp>

#include "lib/glutil/matrix_stack.h"

#include "glhelper/program.h"
#include "glhelper/mesh.h"
#include "constants.h"

#endif
