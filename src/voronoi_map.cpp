/*
 * voronoi_map.cpp
 *
 *  Created on: 20 Apr 2013
 *      Author: adam
 */

#include "voronoi_map.h"

namespace Desper
{

VoronoiMap::VoronoiMap(int numPoints, int width, int height, int relaxCount)
{
    // seed rng first:
    std::uniform_real_distribution<> dist(0, 1);
    rng_type::result_type const seedval = 0; // TODO: get this from somewhere
    m_rng.seed(seedval);

    auto pointList = std::vector<glm::vec2>();
    for (int i = 0; i < numPoints; ++i) {
        auto x = dist(m_rng) * width;
        auto y = dist(m_rng) * height;

        pointList.emplace_back(glm::vec2(x, y));
    }

    voronoi::Voronoi voronoi(pointList, 0, width, 0, height);

    for (int relax = 0; relax < relaxCount; ++relax) {
        pointList = GetCenters(voronoi.sites);
        voronoi = voronoi::Voronoi(pointList, 0, width, 0, height);
    }

    std::vector<glm::vec4> glCenters = std::vector<glm::vec4>();
    std::vector<glm::vec4> glEdges = std::vector<glm::vec4>();

    // assign random colors to edges
    std::vector<glm::vec4> glEdgeColors;
    for (auto i = voronoi.sites.begin(); i != voronoi.sites.end(); i++) {
        voronoi::VSite site = *i;

        glm::vec4 newPoint = glm::vec4(site.coord.x, 0, site.coord.y, 1.0f);
        glCenters.push_back(newPoint);

        Center newCenter = Center(newPoint);
        this->m_polygonCenters.push_back(newCenter);

        glm::vec4 randColor(0.5+(dist(m_rng)/2), 0.5+(dist(m_rng)/2), 0.5+(dist(m_rng)/2), 1.0f);
        // edges are stored as two vec2s in a vec4, xy and zw
        for (auto edge : site.edges)
        {
            glm::vec4 edgePoint1 = glm::vec4(edge.x, 0.0f, edge.y, 1.0f);
            glm::vec4 edgePoint2 = glm::vec4(edge.z, 0.0f, edge.w, 1.0f);

            auto v0 = std::make_shared<Corner>(Corner(edgePoint1));
            auto v1 = std::make_shared<Corner>(Corner(edgePoint2));
            auto newEdge = std::make_shared<Edge>(Edge(v0, v1));

            newCenter.AddBorder(newEdge);

            glEdgeColors.push_back(randColor);
            glEdges.emplace_back(edgePoint1);

            glEdgeColors.push_back(randColor);
            glEdges.emplace_back(edgePoint2);
        }
    }

    std::vector<glm::vec4> glCenterColors;
    glCenterColors.resize(glCenters.size());
    std::fill(glCenterColors.begin(), glCenterColors.begin()+glCenters.size(), glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));

    m_edgeMesh.ReplaceVertices(glEdges, glEdgeColors, glhelper::DrawMode::Edges);
    m_centerMesh.ReplaceVertices(glCenters, glCenterColors, glhelper::DrawMode::Vertices);
}

std::vector<glm::vec2> VoronoiMap::GetCenters(std::list<voronoi::VSite> sites)
{
    std::vector<glm::vec2> centers;

    for (auto site : sites)
    {
        glm::vec2 center;
        for (auto edge : site.edges)
        {
            center += glm::vec2(edge.x, edge.y);
            center += glm::vec2(edge.z, edge.w);
        }
        center /= (site.edges.size() * 2);
        centers.push_back(center);
    }

    return centers;
}

void VoronoiMap::Draw(glhelper::GlProgram program, glutil::MatrixStack modelMatrix)
{
    m_edgeMesh.Draw(program, modelMatrix);
    m_centerMesh.Draw(program, modelMatrix);
}

void VoronoiMap::Update(float dt)
{

}

}
