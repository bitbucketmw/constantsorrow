#include "voronoi_map.h"

namespace Desper
{

VoronoiMap::Center::Center(glm::vec4 coords) : m_coords(coords), m_neighbours(), m_borders(), m_corners() {}

const std::vector<std::shared_ptr<const VoronoiMap::Center> > VoronoiMap::Center::getNeighbours() const {
    return m_neighbours;
}

const std::vector<std::shared_ptr<const VoronoiMap::Edge> > VoronoiMap::Center::getBorders() const {
    return m_borders;
}

const std::vector<std::shared_ptr<const VoronoiMap::Corner> > VoronoiMap::Center::getCorners() const {
    return m_corners;
}

void VoronoiMap::Center::AddBorder(std::shared_ptr<Edge> border)
{
    m_borders.push_back(border);
    m_corners.push_back(border->getv0());
    m_corners.push_back(border->getv1());
}


VoronoiMap::Edge::Edge(std::shared_ptr<Corner> v0, std::shared_ptr<Corner> v1) : m_d0(), m_d1(), m_v0(v0), m_v1(v1) {}

std::shared_ptr<const VoronoiMap::Center> VoronoiMap::Edge::getd0() const {
    return m_d0;
}

std::shared_ptr<const VoronoiMap::Center> VoronoiMap::Edge::getd1() const {
    return m_d1;
}

std::shared_ptr<const VoronoiMap::Corner> VoronoiMap::Edge::getv0() const {
    return m_v0;
}

std::shared_ptr<const VoronoiMap::Corner> VoronoiMap::Edge::getv1() const {
    return m_v1;
}



VoronoiMap::Corner::Corner(glm::vec4 coord) : m_coord(coord), m_touches(), m_protrudes(), m_adjacent() {}

const std::vector<std::shared_ptr<const VoronoiMap::Center> > VoronoiMap::Corner::getTouches() const {
    return m_touches;
}

const std::vector<std::shared_ptr<const VoronoiMap::Edge> > VoronoiMap::Corner::getProtrudes() const {
    return m_protrudes;
}

const std::vector<std::shared_ptr<const VoronoiMap::Corner> > VoronoiMap::Corner::getAdjacent() const {
    return m_adjacent;
}

}
