#pragma once
#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <string>

namespace constants{

const std::string RES_DIRECTORY = "../res/";

}

#endif // CONSTANTS_H
