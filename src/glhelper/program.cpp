/*
 * gl_program.cpp
 *
 *  Created on: 19 Apr 2013
 *      Author: adam
 */

#include "program.h"

namespace glhelper
{
	GlProgram::GlProgram()
	{}

	GlProgram::GlProgram(const std::string &vertexShader, const std::string &fragmentShader)
	{
		std::vector<GLuint> shaderList;
		shaderList.emplace_back(CreateShader(GL_VERTEX_SHADER, LoadShaderFile(vertexShader)));
		shaderList.emplace_back(CreateShader(GL_FRAGMENT_SHADER, LoadShaderFile(fragmentShader)));

		m_program = CreateProgram(shaderList);

		m_positionAttrib = glGetAttribLocation(m_program, "position");
		m_colorAttrib = glGetAttribLocation(m_program, "color");

        m_modelToWorldMatrixUnif = glGetUniformLocation(m_program, "modelToWorldMatrix");
		m_baseColorUnif = glGetUniformLocation(m_program, "baseColor");

		m_globalUniformBlockIdx = glGetUniformBlockIndex(m_program, "GlobalMatrices");
		glGenBuffers(1, &m_globalMatricesUBO);
		glBindBuffer(GL_UNIFORM_BUFFER, m_globalMatricesUBO);
        glBufferData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4), NULL, GL_STREAM_DRAW);
		glBindBuffer(GL_UNIFORM_BUFFER, 0);

		glUniformBlockBinding(m_program, m_globalUniformBlockIdx,
		    m_globalMatricesBindingIdx);
		glBindBufferRange(GL_UNIFORM_BUFFER, m_globalMatricesBindingIdx,
                m_globalMatricesUBO, 0, 2 * sizeof(glm::mat4));
	}

    const GLuint GlProgram::GetProgram() const
    {
        return m_program;
    }

    glhelper::Camera GlProgram::GetCamera() const
    {
        return m_camera;
    }

	void GlProgram::UseCamera(const Camera &camera)
	{
        m_camera = camera;

        glBindBuffer(GL_UNIFORM_BUFFER, m_globalMatricesUBO);
        glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(camera.GetWorldToCameraMat()));
		glBindBuffer(GL_UNIFORM_BUFFER, 0);

    }

    const GLuint GlProgram::GetModelToWorldMatrixUnif() const
    {
        return m_modelToWorldMatrixUnif;
    }

    const GLuint GlProgram::GetGlobalMatricesUBO() const
    {
        return m_globalMatricesUBO;
    }

    const GLuint GlProgram::GetBaseColorUnif() const
    {
        return m_baseColorUnif;
    }

	GLuint GlProgram::CreateProgram(std::vector<GLuint> &shaderList)
	{

		GLuint program = glCreateProgram();
		for (size_t iLoop = 0; iLoop < shaderList.size(); iLoop++)
			glAttachShader(program, shaderList[iLoop]);

		std::for_each(shaderList.begin(), shaderList.end(), glDeleteShader);

		glLinkProgram(program);

		GLint status;
		glGetProgramiv(program, GL_LINK_STATUS, &status);
		if (status == GL_FALSE)
		{
			GLint infoLogLength;
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

			GLchar *infoLog = new GLchar[infoLogLength];
			glGetProgramInfoLog(program, infoLogLength, NULL, infoLog);
			std::fprintf(stderr, "Linker failure: %s\n", infoLog);
			delete[] infoLog;
		}

		for (size_t iLoop = 0; iLoop < shaderList.size(); iLoop++)
			glDetachShader(program, shaderList[iLoop]);

		return program;
	}

	GLuint GlProgram::CreateShader(GLenum shaderType, const std::string &shaderFile)
	{
		GLuint shader = glCreateShader(shaderType);
		const char *shaderFileCString = shaderFile.c_str();
		glShaderSource(shader, 1, &shaderFileCString, NULL);

		glCompileShader(shader);

		GLint status;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
		if (status == GL_FALSE)
		{
			GLint infoLogLength;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);

			GLchar *infoLog = new GLchar[infoLogLength];
			glGetShaderInfoLog(shader, infoLogLength, NULL, infoLog);

			const char *shaderTypeName = NULL;
			switch(shaderType)
			{
			case GL_VERTEX_SHADER: shaderTypeName = "vertex"; break;
			case GL_GEOMETRY_SHADER: shaderTypeName = "shader"; break;
			case GL_FRAGMENT_SHADER: shaderTypeName = "fragment"; break;
			}
			std::fprintf(stderr, "Compile failure in %s shader:\n%s\n", shaderTypeName, infoLog);
			delete[] infoLog;
		}
		return shader;
	}

	std::string GlProgram::LoadShaderFile(const std::string &path)
	{
		std::FILE *file = std::fopen(path.c_str(), "r");
		if (file == nullptr)
		{
			std::cerr << "Error opening file: " << path << std::endl;
			throw errno;
		}

		std::string contents;
		std::fseek(file, 0, SEEK_END);
		contents.resize(std::ftell(file));
		std::rewind(file);
		std::fread(&contents[0], 1, contents.size(), file);
		std::fclose(file);
		return contents;
	}
}
