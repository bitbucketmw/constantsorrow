/*
 * gl_program.h
 *
 *  Created on: 19 Apr 2013
 *      Author: adam
 */

#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include <cstdio>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "camera.h"

namespace glhelper
{
	class GlProgram
	{
	public:
		GlProgram();
		GlProgram(const std::string &vertexShader, const std::string &fragmentShader);
        const GLuint GetProgram() const;

        Camera GetCamera() const;
		void UseCamera(const Camera &camera);

        const GLuint GetGlobalMatricesUBO() const;
        const GLuint GetModelToWorldMatrixUnif() const;
        const GLuint GetBaseColorUnif() const;
    private:
		GLuint CreateProgram(std::vector<GLuint> &shaderList);
		GLuint CreateShader(GLenum shaderType, const std::string &shaderFile);
		std::string LoadShaderFile(const std::string &path);

		GLuint m_positionAttrib = 0;
		GLuint m_colorAttrib = 0;

		GLuint m_program = 0;
        GLuint m_modelToWorldMatrixUnif = 0;
		GLuint m_baseColorUnif = 0;

		GLuint m_globalUniformBlockIdx = 0;
		GLuint m_globalMatricesUBO = 0;
		GLuint m_globalMatricesBindingIdx = 0;

        glhelper::Camera m_camera;
	};
}
