#pragma once

#include <vector>
#include <GL/glew.h>
#include <GL/glfw.h>
#include <GL/glu.h>
#include <glm/glm.hpp>

namespace glhelper
{

enum class DrawMode {
    Edges,
    Vertices
};

class Mesh {
public:
    Mesh() : m_vertices(), m_colors(), m_drawMode(DrawMode::Vertices) {}

    void ReplaceVertices(std::vector<glm::vec4> const& vertices, std::vector<glm::vec4> const& colors, DrawMode drawMode) {
        m_vertices = vertices;
        m_colors = colors;
        m_drawMode = drawMode;


        glGenBuffers(1, &m_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

        glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4) * (vertices.size() + colors.size()),
                NULL, GL_STATIC_DRAW); // for colors and vertices
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(glm::vec4) * vertices.size(), &m_vertices[0]);

        glBufferSubData(GL_ARRAY_BUFFER, sizeof(glm::vec4) * vertices.size(),
                sizeof(glm::vec4) * colors.size(), &m_colors[0]);

        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glGenVertexArrays(1, &m_vao);
        glBindVertexArray(m_vao);

        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);

        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0,  (void*) (sizeof(glm::vec4) * (vertices.size())));

        glBindVertexArray(0);
    }

    void Draw(glhelper::GlProgram program, glutil::MatrixStack modelMatrix) {
        glUseProgram(program.GetProgram());
        glBindVertexArray(m_vao);

        glUniformMatrix4fv(program.GetModelToWorldMatrixUnif(), 1, GL_FALSE, glm::value_ptr(modelMatrix.Top()));

        switch (m_drawMode) {
        case DrawMode::Edges:
            glDrawArrays(GL_LINES, 0, m_vertices.size());
            break;
        case DrawMode::Vertices:
            glDrawArrays(GL_POINTS, 0, m_vertices.size());
            break;
        default:
            break;
        }

        glBindVertexArray(0);
        glUseProgram(0);
    }

private:
    std::vector<glm::vec4> m_vertices;
    std::vector<glm::vec4> m_colors;

    GLuint m_vbo;
    GLuint m_ibo;
    GLuint m_vao;

    DrawMode m_drawMode;
};

}
