/*
 * camera.h
 *
 *  Created on: 21 Apr 2013
 *      Author: adam
 */

#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

namespace glhelper
{
	class Camera
	{
	public:
        Camera(float fovDeg = 20.0f, float zNear = 1.0f, float zFar = 600.0f)
		{
            this->m_zNear = zNear;
            this->m_zFar = zFar;

            float frustumScale = CalcFrustumScale(fovDeg);

            m_worldToCameraMatrix[0].x = frustumScale;
            m_worldToCameraMatrix[1].y = frustumScale;
            m_worldToCameraMatrix[2].z = (zFar + zNear) / (zNear - zFar);
            m_worldToCameraMatrix[2].w = -1.0f;
            m_worldToCameraMatrix[3].z = (2 * zFar * zNear) / (zNear - zFar);

            m_worldToCameraMatrix = glm::rotate(glm::translate(m_worldToCameraMatrix, glm::vec3(0,1,-1)), 25.0f, glm::vec3(1.0f, 0, 0));
            m_worldToCameraMatrix = glm::lookAt(
                glm::vec3(4.0f,2.0f,0), // Camera position in World Space
                glm::vec3(0,0,0), // and looks at the origin
                glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
            );
		}

        glm::mat4 GetWorldToCameraMat() const
		{
            return m_worldToCameraMatrix;
		}

		void ApplyTransform(glm::mat4 transform)
		{
            m_worldToCameraMatrix *= transform;
		}

        float m_zNear;
        float m_zFar;

	private:
        glm::mat4 m_worldToCameraMatrix = glm::mat4(1);

		float CalcFrustumScale(float fFovDeg)
		{
			const float degToRad = 3.14159f * 2.0f / 360.0f;
			float fFovRad = fFovDeg * degToRad;
			return 1.0f / tan(fFovRad / 2.0f);
		}
	};
}
