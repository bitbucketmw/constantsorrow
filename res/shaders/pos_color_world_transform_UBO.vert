#version 330
layout(location = 0) in vec4 position;
layout(location = 1) in vec4 color;

layout(std140) uniform GlobalMatrices
{
	mat4 cameraToClipMatrix;
	mat4 worldToCameraMatrix;
};

uniform mat4 modelToWorldMatrix;

smooth out vec4 fragColor;

void main()
{
	if (worldToCameraMatrix[2][0] == 0) {
		gl_Position = position;
	}
	else {
		gl_Position = (cameraToClipMatrix * (worldToCameraMatrix * (modelToWorldMatrix * position)));
	}
	fragColor = color;
}
