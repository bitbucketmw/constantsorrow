#version 330

smooth in vec4 fragColor;
out vec4 outColor;

void main()
{
	outColor = fragColor;
}
