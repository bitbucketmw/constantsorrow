#version 330

smooth in vec4 fragColor;
out vec4 outputColor;

uniform vec4 baseColor = vec4(1.0, 1.0, 1.0, 1.0);

void main()
{
	outputColor = fragColor * baseColor;
}