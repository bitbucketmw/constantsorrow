cmake_minimum_required (VERSION 2.6)
project (ConstantSorrow)

SET(CMAKE_BUILD_TYPE Debug)

file(GLOB_RECURSE SRC_LIST ./src/*.cpp ./src/*.h)
list(APPEND CMAKE_CXX_FLAGS "-std=c++0x ${CMAKE_CXX_FLAGS}")

if(MSVC)
  # Force to always compile with W4
  if(CMAKE_CXX_FLAGS MATCHES "/W[0-4]")
    string(REGEX REPLACE "/W[0-4]" "/W4" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
  else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")
  endif()
elseif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
  # Update if necessary
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wno-long-long")
endif()

add_executable(ConstantSorrow ${SRC_LIST})

target_link_libraries (ConstantSorrow GL)
target_link_libraries (ConstantSorrow GLEW)
target_link_libraries (ConstantSorrow glfw)
